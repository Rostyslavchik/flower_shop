package com.rostyslavprotsiv.model.action.strategy.packing;

public interface IPacking {
    void pack();
}
