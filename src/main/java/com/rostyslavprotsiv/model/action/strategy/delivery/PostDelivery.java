package com.rostyslavprotsiv.model.action.strategy.delivery;

public class PostDelivery implements IDelivery {
    public void deliver() {
        System.out.println("Deliver with a post");
    }
}
