package com.rostyslavprotsiv.model.action.strategy.delivery;

public interface IDelivery {
    void deliver();
}
