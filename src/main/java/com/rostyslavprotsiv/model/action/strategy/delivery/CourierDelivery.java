package com.rostyslavprotsiv.model.action.strategy.delivery;

public class CourierDelivery implements IDelivery {
    public void deliver() {
        System.out.println("Deliver with a courier");
    }
}
