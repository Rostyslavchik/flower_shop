package com.rostyslavprotsiv.model.action.strategy.packing;

public class OrdinaryPacking implements IPacking {
    public void pack() {
        System.out.println("Pack as usually");
    }
}
