package com.rostyslavprotsiv.model.action.strategy.packing;

public class VipPacking implements IPacking {
    public void pack() {
        System.out.println("Pack with VIP package");
    }
}
