package com.rostyslavprotsiv.model.entity;

import java.util.Objects;

public class Client { // for observer and adapter(card types)
    private int id;
    private String name;
    private int bonuses;
    private String email;

    public Client(int id, String name, int bonuses, String email) {
        this.id = id;
        this.name = name;
        this.bonuses = bonuses;
        this.email = email;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getBonuses() {
        return bonuses;
    }

    public void addBonuses(int bonuses) {
        this.bonuses += bonuses;
    }

    public void setBonuses(int bonuses) {
        this.bonuses = bonuses;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Client client = (Client) o;
        return id == client.id && bonuses == client.bonuses && Objects.equals(name, client.name) && Objects.equals(email, client.email);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, name, bonuses, email);
    }

    @Override
    public String toString() {
        return "Client{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", bonuses=" + bonuses +
                ", email='" + email + '\'' +
                '}';
    }
}
