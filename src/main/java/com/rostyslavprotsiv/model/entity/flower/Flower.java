package com.rostyslavprotsiv.model.entity.flower;

import com.rostyslavprotsiv.model.exception.FlowerLogicalException;

public abstract class Flower {
    private String budColour;
    private double weight;
    private int price;

    public Flower(String bubColour, double weight, int price)
            throws FlowerLogicalException {
        if (checkWeight(weight) && checkPrice(price)) {
            this.budColour = bubColour;
            this.weight = weight;
            this.price = price;
        } else {
            throw new FlowerLogicalException("The weight "
                    + "or price of flower is bad!!");
        }
    }

    public String getBubColour() {
        return budColour;
    }

    public void setBubColour(String bubColour) {
        this.budColour = bubColour;
    }

    public double getWeight() {
        return weight;
    }

    public void setWeight(double weight) throws FlowerLogicalException {
        if (checkWeight(weight)) {
            this.weight = weight;
        } else {
            throw new FlowerLogicalException("The weight of flower is bad!!");
        }
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) throws FlowerLogicalException {
        if (checkPrice(price)) {
            this.price = price;
        } else {
            throw new FlowerLogicalException("Price of flower is bad!!");
        }
    }

    private boolean checkWeight(double weight) {
        return weight > 0 && weight < 999;
    }

    private boolean checkPrice(int price) {
        return price > 0;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        Flower other = (Flower) obj;
        if (budColour == null) {
            if (other.budColour != null) {
                return false;
            }
        } else if (!budColour.equals(other.budColour)) {
            return false;
        }
        if (weight != other.weight) {
            return false;
        }
        if (price != other.price) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (int)(((budColour == null) ? 0 : budColour.hashCode())
                + 34.5 * weight + 46.56 * price);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '@' + "budColour = " + budColour
                + ", weight = " + weight + ", price = " + price;
    }
}
