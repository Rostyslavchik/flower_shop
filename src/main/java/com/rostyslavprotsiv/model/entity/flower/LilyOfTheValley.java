package com.rostyslavprotsiv.model.entity.flower;

import com.rostyslavprotsiv.model.exception.FlowerLogicalException;

import java.util.Objects;

public class LilyOfTheValley extends Flower {
    private int numOfBells;

    public LilyOfTheValley(String bubColour, double weight, int price,
                           int numOfBells) throws FlowerLogicalException {
        super(bubColour, weight, price);
        this.numOfBells = numOfBells;
    }

    public int getNumOfBells() {
        return numOfBells;
    }

    public void setNumOfBells(int numOfBells) {
        this.numOfBells = numOfBells;
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        LilyOfTheValley that = (LilyOfTheValley) o;
        return numOfBells == that.numOfBells;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), numOfBells);
    }

    @Override
    public String toString() {
        return super.toString() + ", numOfBells=" + numOfBells;
    }
}
