package com.rostyslavprotsiv.model.entity.flower;

import com.rostyslavprotsiv.model.exception.FlowerLogicalException;

import java.util.Objects;

public class Sunflower extends Flower {
    private double height;

    public Sunflower(String bubColour, double weight, int price,
                     double height) throws FlowerLogicalException {
        super(bubColour, weight, price);
        this.height = height;
    }

    public double getHeight() {
        return height;
    }

    public void setHeight(double height) {
        this.height = height;
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        Sunflower sunflower = (Sunflower) o;
        return Double.compare(sunflower.height, height) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), height);
    }

    @Override
    public String toString() {
        return super.toString() + ", height=" + height;
    }
}
