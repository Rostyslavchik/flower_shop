package com.rostyslavprotsiv.model.entity.flower;

import com.rostyslavprotsiv.model.exception.FlowerLogicalException;

public class Rose extends Flower {
    private boolean hasSpikes;

    public Rose(String bubColour, double weight, int price,
                boolean hasSpikes) throws FlowerLogicalException {
        super(bubColour, weight, price);
        this.hasSpikes = hasSpikes;
    }

    public boolean isHasSpikes() {
        return hasSpikes;
    }

    public void setHasSpikes(boolean hasSpikes) {
        this.hasSpikes = hasSpikes;
    }

    @Override
    public boolean equals(Object obj) {
        if (!super.equals(obj)) {
            return false;
        }
        Rose other = (Rose) obj;
        if (hasSpikes != other.hasSpikes) {
            return false;
        }
        return true;
    }

    @Override
    public int hashCode() {
        return (int)(super.hashCode() + 44.67 * (hasSpikes ? 1.4 : 4.6));
    }

    @Override
    public String toString() {
        return super.toString() + " ,hasSpikes = " + hasSpikes;
    }
}
