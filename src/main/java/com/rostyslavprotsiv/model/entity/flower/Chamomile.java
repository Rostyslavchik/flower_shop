package com.rostyslavprotsiv.model.entity.flower;

import com.rostyslavprotsiv.model.exception.FlowerLogicalException;

import java.util.Objects;

public class Chamomile extends Flower {
    private int age;
    private double budRadius;

    public Chamomile(String bubColour, double weight, int price,
                     int age, double budRadius) throws FlowerLogicalException {
        super(bubColour, weight, price);
        this.age = age;
        this.budRadius = budRadius;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public double getBudRadius() {
        return budRadius;
    }

    public void setBudRadius(double budRadius) {
        this.budRadius = budRadius;
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        Chamomile chamomile = (Chamomile) o;
        return age == chamomile.age
                && Double.compare(chamomile.budRadius, budRadius) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), age, budRadius);
    }

    @Override
    public String toString() {
        return super.toString() + ", age=" + age + ", budRadius=" + budRadius;
    }
}
