package com.rostyslavprotsiv.model.entity.bouquet;

import com.rostyslavprotsiv.model.action.strategy.delivery.IDelivery;
import com.rostyslavprotsiv.model.action.strategy.packing.IPacking;
import com.rostyslavprotsiv.model.entity.flower.Flower;

import java.util.List;
import java.util.Objects;

public abstract class Bouquet implements IBouquet {
    protected IDelivery delivery;
    protected IPacking packing;
    private List<Flower> flowers;

    public Bouquet(List<Flower> flowers, IDelivery delivery, IPacking packing) {
        this.delivery = delivery;
        this.packing = packing;
        this.flowers = flowers;
    }

    public List<Flower> getFlowers() {
        return flowers;
    }

    public void setFlowers(List<Flower> flowers) {
        this.flowers = flowers;
    }

    public IDelivery getDelivery() {
        return delivery;
    }

    public void setDelivery(IDelivery delivery) {
        this.delivery = delivery;
    }

    public IPacking getPacking() {
        return packing;
    }

    public void setPacking(IPacking packing) {
        this.packing = packing;
    }

    @Override
    public double computeTotalPrice() { // override in some classes
        return flowers.stream().mapToDouble(Flower::getPrice).sum();
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Bouquet bouquet = (Bouquet) o;
        return Objects.equals(delivery, bouquet.delivery)
                && Objects.equals(packing, bouquet.packing)
                && Objects.equals(flowers, bouquet.flowers);
    }

    @Override
    public int hashCode() {
        return Objects.hash(delivery, packing, flowers);
    }

    @Override
    public String toString() {
        return getClass().getSimpleName() + '@' +
                "flowers=" + flowers + ", delivery=" + delivery + ", packing="
                + packing;
    }
}
