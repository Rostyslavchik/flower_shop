package com.rostyslavprotsiv.model.entity.bouquet;

import com.rostyslavprotsiv.model.action.strategy.delivery.IDelivery;
import com.rostyslavprotsiv.model.action.strategy.packing.IPacking;
import com.rostyslavprotsiv.model.entity.flower.Flower;

import java.util.List;
import java.util.Objects;

public class BouquetFuneralWork extends Bouquet {
    private int blackTapesNum;

    public BouquetFuneralWork(List<Flower> flowers, IDelivery delivery,
                              IPacking packing, int blackTapesNum) {
        super(flowers, delivery, packing);
        this.blackTapesNum = blackTapesNum;
    }

    public int getBlackTapesNum() {
        return blackTapesNum;
    }

    public void setBlackTapesNum(int blackTapesNum) {
        this.blackTapesNum = blackTapesNum;
    }

    @Override
    public int prepareBefore() {
        System.out.println("Preparing Funeral Work Bouquet!");
        return 1;
    }

    @Override
    public void pack() {
        packing.pack();
    }

    @Override
    public void compose() {
        System.out.println("Composing of funeral work bouquet");
    }

    @Override
    public int deliver() {
        delivery.deliver();
        return 1;
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        BouquetFuneralWork that = (BouquetFuneralWork) o;
        return blackTapesNum == that.blackTapesNum;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), blackTapesNum);
    }

    @Override
    public String toString() {
        return super.toString() +
                ", blackTapesNum=" + blackTapesNum;
    }
}
