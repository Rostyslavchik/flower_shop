package com.rostyslavprotsiv.model.entity.bouquet;

public enum BouquetType {
    BIRTHDAY_FANCY, BIRTHDAY_FRIEND, BIRTHDAY_LOVE,
    FUNERAL_WORK, FUNERAL_FRIEND,
    VALENTINES_DAY_LOVE,
    WEDDING_BRO
}
