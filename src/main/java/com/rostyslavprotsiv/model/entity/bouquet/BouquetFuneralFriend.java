package com.rostyslavprotsiv.model.entity.bouquet;

import com.rostyslavprotsiv.model.action.strategy.delivery.IDelivery;
import com.rostyslavprotsiv.model.action.strategy.packing.IPacking;
import com.rostyslavprotsiv.model.entity.flower.Flower;

import java.util.List;

public class BouquetFuneralFriend extends Bouquet {
    public BouquetFuneralFriend(List<Flower> flowers,
                                IDelivery delivery, IPacking packing) {
        super(flowers, delivery, packing);
    }

    @Override
    public int prepareBefore() {
        System.out.println("Preparing Funeral Friend Bouquet!");
        return 1;
    }

    @Override
    public void pack() {
        packing.pack();
    }

    @Override
    public void compose() {
        System.out.println("Composing of funeral friend bouquet");
    }

    @Override
    public int deliver() {
        delivery.deliver();
        return 1;
    }
}
