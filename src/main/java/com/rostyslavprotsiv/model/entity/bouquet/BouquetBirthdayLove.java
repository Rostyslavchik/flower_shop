package com.rostyslavprotsiv.model.entity.bouquet;

import com.rostyslavprotsiv.model.action.strategy.delivery.IDelivery;
import com.rostyslavprotsiv.model.action.strategy.packing.IPacking;
import com.rostyslavprotsiv.model.entity.flower.Flower;

import java.util.List;
import java.util.Objects;

public class BouquetBirthdayLove extends Bouquet {
    private boolean withPaperHeart;
    private static final double PAPER_HEART_PRICE = 2;

    public BouquetBirthdayLove(List<Flower> flowers, IDelivery delivery,
                               IPacking packing, boolean withPaperHeart) {
        super(flowers, delivery, packing);
        this.withPaperHeart = withPaperHeart;
    }

    public boolean isWithPaperHeart() {
        return withPaperHeart;
    }

    public void setWithPaperHeart(boolean withPaperHeart) {
        this.withPaperHeart = withPaperHeart;
    }

    @Override
    public int prepareBefore() {
        System.out.println("Preparing Love Birthday Bouquet!");
        return 1;
    }

    @Override
    public void pack() {
        packing.pack();
    }

    @Override
    public void compose() {
        System.out.println("Composing of birthday love bouquet");
    }

    @Override
    public int deliver() {
        delivery.deliver();
        return 1;
    }

    @Override
    public double computeTotalPrice() {
        double price = super.computeTotalPrice();
        return (price + (withPaperHeart ? PAPER_HEART_PRICE : 0));
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        BouquetBirthdayLove that = (BouquetBirthdayLove) o;
        return withPaperHeart == that.withPaperHeart;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), withPaperHeart);
    }

    @Override
    public String toString() {
        return super.toString() +
                ", withPaperHeart= " + withPaperHeart;
    }
}
