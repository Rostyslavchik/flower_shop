package com.rostyslavprotsiv.model.entity.bouquet;

public interface IBouquet {
    int prepareBefore();

    void pack();//strategy pattern / decorator pattern

    void compose();

    int deliver();//strategy pattern / decorator pattern

    double computeTotalPrice();//decorator
}
