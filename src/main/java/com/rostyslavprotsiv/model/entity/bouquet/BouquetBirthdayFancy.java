package com.rostyslavprotsiv.model.entity.bouquet;

import com.rostyslavprotsiv.model.action.strategy.delivery.IDelivery;
import com.rostyslavprotsiv.model.action.strategy.packing.IPacking;
import com.rostyslavprotsiv.model.entity.flower.Flower;
import com.rostyslavprotsiv.model.exception.BouquetLogicalException;

import java.util.List;
import java.util.Objects;

public class BouquetBirthdayFancy extends Bouquet {
    private int fancyPercents;

    public BouquetBirthdayFancy(List<Flower> flowers, IDelivery delivery,
                                IPacking packing, int fancyPercents)
            throws BouquetLogicalException {
        super(flowers, delivery, packing);
        if (checkFancyPercents(fancyPercents)) {
            this.fancyPercents = fancyPercents;
        } else {
            throw new BouquetLogicalException("%Fancy < 0 or > 100");
        }
    }

    public int getFancyPercents() {
        return fancyPercents;
    }

    public void setFancyPercents(int fancyPercents)
            throws BouquetLogicalException {
        if (checkFancyPercents(fancyPercents)) {
            this.fancyPercents = fancyPercents;
        } else {
            throw new BouquetLogicalException("%Fancy < 0 or > 100");
        }
    }

    private boolean checkFancyPercents(int fancyPercents) {
        return fancyPercents > 0 && fancyPercents < 100; // magic numbers
    }


    @Override
    public int prepareBefore() {
        System.out.println("Preparing Fancy Birthday Bouquet!");
        return 1;
    }

    @Override
    public void pack() {
        packing.pack();
    }

    @Override
    public void compose() {
        System.out.println("Composing of birthday fancy bouquet");
    }

    @Override
    public int deliver() {
        delivery.deliver();
        return 1;
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        BouquetBirthdayFancy that = (BouquetBirthdayFancy) o;
        return fancyPercents == that.fancyPercents;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), fancyPercents);
    }

    @Override
    public String toString() {
        return super.toString() +
                ", fancyPercents=" + fancyPercents;
    }
}
