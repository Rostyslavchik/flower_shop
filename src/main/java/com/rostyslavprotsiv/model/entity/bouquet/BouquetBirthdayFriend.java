package com.rostyslavprotsiv.model.entity.bouquet;

import com.rostyslavprotsiv.model.action.strategy.delivery.IDelivery;
import com.rostyslavprotsiv.model.action.strategy.packing.IPacking;
import com.rostyslavprotsiv.model.entity.flower.Flower;

import java.util.List;
import java.util.Objects;

public class BouquetBirthdayFriend extends Bouquet {
    private boolean withNote;

    public BouquetBirthdayFriend(List<Flower> flowers, IDelivery delivery,
                                 IPacking packing, boolean withNote) {
        super(flowers, delivery, packing);
        this.withNote = withNote;
    }

    public boolean isWithNote() {
        return withNote;
    }

    public void setWithNote(boolean withNote) {
        this.withNote = withNote;
    }

    @Override
    public int prepareBefore() {
        System.out.println("Preparing Birthday Bouquet for friend!");
        return 1;
    }

    @Override
    public void pack() {
        packing.pack();
    }

    @Override
    public void compose() {
        System.out.println("Composing of birthday bouquet for friend");
    }

    @Override
    public int deliver() {
        delivery.deliver();
        return 1;
    }

    @Override
    public boolean equals(Object o) {
        if (!super.equals(o)) return false;
        BouquetBirthdayFriend that = (BouquetBirthdayFriend) o;
        return withNote == that.withNote;
    }

    @Override
    public int hashCode() {
        return Objects.hash(super.hashCode(), withNote);
    }

    @Override
    public String toString() {
        return super.toString() +
                ", withNote=" + withNote;
    }
}
