package com.rostyslavprotsiv.model.entity.adapter;

import com.rostyslavprotsiv.model.entity.adapter.adaptee.ISpecificClientCard;
import com.rostyslavprotsiv.model.entity.adapter.targett.IClientCard;

public class CardConverter implements IClientCard {
    private ISpecificClientCard specificClientCard;
    private static final int DOLLARS_PER_BONUS = 2;

    public CardConverter(ISpecificClientCard specificClientCard) {
        this.specificClientCard = specificClientCard;
    }

    @Override
    public double getDiscount() {
        return specificClientCard.getSocialBonuses() * DOLLARS_PER_BONUS;
    }
}
