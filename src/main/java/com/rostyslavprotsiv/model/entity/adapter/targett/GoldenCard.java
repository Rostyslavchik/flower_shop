package com.rostyslavprotsiv.model.entity.adapter.targett;

import java.util.Objects;

public class GoldenCard implements IClientCard {
    private double freeDeliveryPrice;

    public GoldenCard(double freeDeliveryPrice) {
        this.freeDeliveryPrice = freeDeliveryPrice;
    }

    public double getFreeDeliveryPrice() {
        return freeDeliveryPrice;
    }

    public void setFreeDeliveryPrice(double freeDeliveryPrice) {
        this.freeDeliveryPrice = freeDeliveryPrice;
    }

    @Override
    public double getDiscount() {
        return -freeDeliveryPrice;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        GoldenCard that = (GoldenCard) o;
        return Double.compare(that.freeDeliveryPrice, freeDeliveryPrice) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(freeDeliveryPrice);
    }

    @Override
    public String toString() {
        return "GoldenCard{" +
                "freeDeliveryPrice=" + freeDeliveryPrice +
                '}';
    }
}
