package com.rostyslavprotsiv.model.entity.adapter.adaptee;

public interface ISpecificClientCard {
    int getSocialBonuses();
}
