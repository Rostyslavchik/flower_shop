package com.rostyslavprotsiv.model.entity.adapter.targett;

import java.util.Objects;

public class BonusCard implements IClientCard {
    private int bonuses;
    private static final double DOLLARS_PER_BONUS = 0.12;

    public BonusCard(int bonuses) {
        this.bonuses = bonuses;
    }

    public int getBonuses() {
        return bonuses;
    }

    public void setBonuses(int bonuses) {
        this.bonuses = bonuses;
    }

    @Override
    public double getDiscount() {
        return bonuses * DOLLARS_PER_BONUS;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        BonusCard bonusCard = (BonusCard) o;
        return bonuses == bonusCard.bonuses;
    }

    @Override
    public int hashCode() {
        return Objects.hash(bonuses);
    }

    @Override
    public String toString() {
        return "BonusCard{" +
                "bonuses=" + bonuses +
                '}';
    }
}
