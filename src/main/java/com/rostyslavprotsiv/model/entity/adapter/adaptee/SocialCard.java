package com.rostyslavprotsiv.model.entity.adapter.adaptee;

import java.util.Objects;

public class SocialCard implements ISpecificClientCard {
    private int socialBonuses;

    public SocialCard(int socialBonuses) {
        this.socialBonuses = socialBonuses;
    }

    public void setSocialBonuses(int socialBonuses) {
        this.socialBonuses = socialBonuses;
    }

    @Override
    public int getSocialBonuses() {
        return socialBonuses;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        SocialCard that = (SocialCard) o;
        return socialBonuses == that.socialBonuses;
    }

    @Override
    public int hashCode() {
        return Objects.hash(socialBonuses);
    }

    @Override
    public String toString() {
        return "SocialCard{" +
                "socialBonuses=" + socialBonuses +
                '}';
    }
}
