package com.rostyslavprotsiv.model.entity.adapter.targett;

public interface IClientCard {
    double getDiscount();
}
