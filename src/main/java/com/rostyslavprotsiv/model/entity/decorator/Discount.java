package com.rostyslavprotsiv.model.entity.decorator;

public class Discount extends BouquetDecorator {
    private static final double DISCOUNT = 30;

    public Discount() {
        setAdditionalPrice(-DISCOUNT);
    }

    @Override
    public void compose() {
        super.compose();
        discountComposing();
    }

    private void discountComposing() {
        System.out.println("Assembling with medium quality because of discount");
    }
}
