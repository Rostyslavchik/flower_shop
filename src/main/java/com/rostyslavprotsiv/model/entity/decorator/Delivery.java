package com.rostyslavprotsiv.model.entity.decorator;

import java.time.Period;

public class Delivery extends BouquetDecorator {
    private static final double DELIVERY_PRICE = 33.50;
    private Period deliveryTime;

    public Delivery(Period deliveryTime) {
        setAdditionalPrice(DELIVERY_PRICE);
        this.deliveryTime = deliveryTime;
    }

    @Override
    public int deliver() {
        int result = super.deliver();
        System.out.print(" + Delivery time: " + deliveryTime);
        return result;
    }
}
