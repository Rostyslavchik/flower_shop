package com.rostyslavprotsiv.model.entity.decorator;

import com.rostyslavprotsiv.model.entity.bouquet.Bouquet;

public class Packing extends BouquetDecorator {
    private static final double PACKING_PRICE = 12.40;
    private PackingType packingType;

    public Packing(PackingType packingType) {
        this.packingType = packingType;
        setAdditionalPrice(PACKING_PRICE);
    }

    @Override
    public void pack() {
        super.pack();
        System.out.print(" + Packing Type:" + packingType);
    }
}
