package com.rostyslavprotsiv.model.entity.decorator;

import com.rostyslavprotsiv.model.entity.bouquet.Bouquet;
import com.rostyslavprotsiv.model.entity.bouquet.IBouquet;

import java.time.Period;
import java.util.Optional;

public abstract class BouquetDecorator implements IBouquet {
    protected Optional<IBouquet> bouquet;
    private double additionalPrice;

    public void setBouquet(IBouquet bouquet) {
        this.bouquet = Optional.ofNullable(bouquet);
    }

    protected void setAdditionalPrice(double additionalPrice) {
        this.additionalPrice = additionalPrice;
    }

    @Override
    public int prepareBefore() {
        return bouquet.orElseThrow(IllegalArgumentException::new)
                .prepareBefore();
    }

    @Override
    public void pack() {
        bouquet.orElseThrow(IllegalArgumentException::new).pack();
    }

    @Override
    public void compose() {
        bouquet.orElseThrow(IllegalArgumentException::new).compose();
    }

    @Override
    public int deliver() {
        return bouquet.orElseThrow(IllegalArgumentException::new)
                .deliver();
    }

    @Override
    public double computeTotalPrice() {
       return bouquet.orElseThrow(IllegalArgumentException::new)
               .computeTotalPrice() + additionalPrice;
    }
}
