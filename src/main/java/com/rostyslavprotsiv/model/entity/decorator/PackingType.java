package com.rostyslavprotsiv.model.entity.decorator;

public enum PackingType {
    PAPER, BOX, DUCT_TAPE
}
