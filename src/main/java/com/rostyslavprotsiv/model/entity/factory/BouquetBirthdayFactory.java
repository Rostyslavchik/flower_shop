package com.rostyslavprotsiv.model.entity.factory;

import com.rostyslavprotsiv.model.action.strategy.delivery.CourierDelivery;
import com.rostyslavprotsiv.model.action.strategy.delivery.PostDelivery;
import com.rostyslavprotsiv.model.action.strategy.packing.OrdinaryPacking;
import com.rostyslavprotsiv.model.action.strategy.packing.VipPacking;
import com.rostyslavprotsiv.model.entity.bouquet.*;
import com.rostyslavprotsiv.model.entity.flower.*;
import com.rostyslavprotsiv.model.exception.BouquetLogicalException;
import com.rostyslavprotsiv.model.exception.FlowerLogicalException;

import java.util.Arrays;

public class BouquetBirthdayFactory extends BouquetFactory {
    @Override
    protected IBouquet createBouquet(BouquetType type) {
        switch (type) {
            case BIRTHDAY_FANCY:
                try {
                    return new BouquetBirthdayFancy(Arrays.asList(
                            new Flower[]{
                                    new Chamomile("red", 15, 55, 3, 4),
                                    new Chamomile("blue", 15, 58, 2, 4),
                                    new Rose("red", 10, 20, true),
                                    new Sunflower("yellow", 10, 20, 14)}
                                                     ),
                            new PostDelivery(),
                            new OrdinaryPacking(),
                            55);
                } catch (BouquetLogicalException | FlowerLogicalException e) {
                    e.printStackTrace();
                }
            case BIRTHDAY_FRIEND:
                try {
                    return new BouquetBirthdayFriend(Arrays.asList(
                            new Flower[]{
                                    new Chamomile("white", 32, 100, 3, 4),
                                    new LilyOfTheValley("black", 1, 5, 2),
                                    new Sunflower("yellow", 10, 20, 14)}
                                                    ),
                            new PostDelivery(),
                            new VipPacking(),
                            true);
                } catch (FlowerLogicalException e) {
                    e.printStackTrace();
                }
            case BIRTHDAY_LOVE:
                try {
                    return new BouquetBirthdayLove(Arrays.asList(
                            new Flower[]{
                                    new Chamomile("white", 32, 100, 3, 4),
                                    new LilyOfTheValley("black", 1, 5, 2),
                                    new Sunflower("yellow", 10, 20, 14),
                                    new Rose("red", 14, 39, true),
                                    new Rose("orange", 14, 23, false)}
                                                    ),
                            new CourierDelivery(),
                            new VipPacking(),
                            true);
                } catch (FlowerLogicalException e) {
                    e.printStackTrace();
                }
            default:
                throw new EnumConstantNotPresentException(
                        type.getDeclaringClass(), type.name());
        }
    }
}
