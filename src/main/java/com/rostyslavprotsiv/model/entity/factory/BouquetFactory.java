package com.rostyslavprotsiv.model.entity.factory;

import com.rostyslavprotsiv.model.entity.bouquet.BouquetType;
import com.rostyslavprotsiv.model.entity.bouquet.IBouquet;

public abstract class BouquetFactory {
    protected abstract IBouquet createBouquet(BouquetType type);

    public IBouquet compose(BouquetType type) {
        IBouquet bouquet = createBouquet(type);
        bouquet.prepareBefore();
        bouquet.compose();
        return bouquet;
    }
}
