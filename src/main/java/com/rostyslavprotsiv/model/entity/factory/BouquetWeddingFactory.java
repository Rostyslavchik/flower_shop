package com.rostyslavprotsiv.model.entity.factory;

import com.rostyslavprotsiv.model.action.strategy.delivery.CourierDelivery;
import com.rostyslavprotsiv.model.action.strategy.packing.OrdinaryPacking;
import com.rostyslavprotsiv.model.entity.bouquet.*;
import com.rostyslavprotsiv.model.entity.flower.*;
import com.rostyslavprotsiv.model.exception.FlowerLogicalException;

import java.util.Arrays;

public class BouquetWeddingFactory extends BouquetFactory {
    @Override
    protected IBouquet createBouquet(BouquetType type) {
        switch (type) {
            case WEDDING_BRO:
                try {
                    return new BouquetWeddingBro(Arrays.asList(
                            new Flower[]{
                                    new Chamomile("red", 15, 55, 3, 4),
                                    new Chamomile("blue", 15, 14, 4, 4),
                                    new Rose("pink", 11, 10, true),
                                    new Sunflower("yellow", 10, 20, 14),
                                    new Sunflower("orange", 13, 25, 16),
                                    new Sunflower("orange", 13, 25, 17)}
                                                ),
                            new CourierDelivery(),
                            new OrdinaryPacking());
                } catch (FlowerLogicalException e) {
                    e.printStackTrace();
                }
            default:
                throw new EnumConstantNotPresentException(
                        type.getDeclaringClass(), type.name());
        }
    }
}
