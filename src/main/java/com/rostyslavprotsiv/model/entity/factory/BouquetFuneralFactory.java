package com.rostyslavprotsiv.model.entity.factory;

import com.rostyslavprotsiv.model.action.strategy.delivery.PostDelivery;
import com.rostyslavprotsiv.model.action.strategy.packing.OrdinaryPacking;
import com.rostyslavprotsiv.model.entity.bouquet.*;
import com.rostyslavprotsiv.model.entity.flower.*;
import com.rostyslavprotsiv.model.exception.FlowerLogicalException;

import java.util.Arrays;

public class BouquetFuneralFactory extends BouquetFactory{
    @Override
    protected IBouquet createBouquet(BouquetType type) {
        switch (type) {
            case FUNERAL_FRIEND:
                try {
                    return new BouquetFuneralFriend(Arrays.asList(
                            new Flower[]{
                                    new Chamomile("blue", 9, 34, 6, 3),
                                    new Chamomile("brown", 34, 26, 5, 7),
                                    new Rose("red", 10, 20, false),
                                    new LilyOfTheValley("purple", 3, 25, 4),
                                    new LilyOfTheValley("purple", 4, 35, 2)
                            }
                                                   ),
                            new PostDelivery(),
                            new OrdinaryPacking());
                } catch (FlowerLogicalException e) {
                    e.printStackTrace();
                }
            case FUNERAL_WORK:
                try {
                    return new BouquetFuneralWork(Arrays.asList(
                            new Flower[]{
                                    new Chamomile("yellow", 33, 105, 3, 4),
                                    new LilyOfTheValley("black", 1, 8, 2)
                            }
                                                 ),
                            new PostDelivery(),
                            new OrdinaryPacking(),
                            4);
                } catch (FlowerLogicalException e) {
                    e.printStackTrace();
                }
            default:
                throw new EnumConstantNotPresentException(
                        type.getDeclaringClass(), type.name());
        }
    }
}
