package com.rostyslavprotsiv.model.entity.factory;

import com.rostyslavprotsiv.model.action.strategy.delivery.PostDelivery;
import com.rostyslavprotsiv.model.action.strategy.packing.VipPacking;
import com.rostyslavprotsiv.model.entity.bouquet.*;
import com.rostyslavprotsiv.model.entity.flower.*;
import com.rostyslavprotsiv.model.exception.FlowerLogicalException;

import java.util.Arrays;

public class BouquetValentinesDayFactory extends BouquetFactory {
    @Override
    protected IBouquet createBouquet(BouquetType type) {
        switch (type) {
            case VALENTINES_DAY_LOVE:
                try {
                    return new BouquetValentinesDayLove(Arrays.asList(
                            new Flower[]{
                                    new Rose("red", 11, 20, true),
                                    new Rose("red", 13, 20, true),
                                    new Rose("red", 11, 20, true),
                                    new Rose("red", 12, 20, true),
                                    new Sunflower("yellow", 10, 18, 12)}
                    ),
                            new PostDelivery(),
                            new VipPacking());
                } catch (FlowerLogicalException e) {
                    e.printStackTrace();
                }
            default:
                throw new EnumConstantNotPresentException(
                        type.getDeclaringClass(), type.name());
        }
    }
}
