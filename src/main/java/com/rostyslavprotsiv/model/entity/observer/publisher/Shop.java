package com.rostyslavprotsiv.model.entity.observer.publisher;

import com.rostyslavprotsiv.model.entity.Client;
import com.rostyslavprotsiv.model.entity.bouquet.IBouquet;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

public class Shop { // Observer pattern
    private EventManager manager;
    private List<IBouquet> bouquets;

    public Shop() {
        this.manager = new EventManager("new", "ready", "bonuses");
        bouquets = new LinkedList<>();
    }

    public List<IBouquet> getBouquets() {
        return bouquets;
    }

    public void setBouquets(List<IBouquet> bouquets) {
        this.bouquets = bouquets;
    }

    public EventManager getManager() {
        return manager;
    }

    public void addNewBouquet(IBouquet bouquet) {
        Class<? extends IBouquet> clazz = bouquet.getClass();
        for (IBouquet bq : bouquets) {
            if (bq.getClass() == clazz) {
                bouquets.add(bouquet);
                return;
            }
        }
        manager.notify("new", bouquet, -1, 0);
        bouquets.add(bouquet);
    }

    public void addReadyBouquet(IBouquet bouquet, int clientID) {
        manager.notify("ready", bouquet, clientID, 0);
        bouquets.add(bouquet);
    }

    public void addBonus(int clientID, int bonus) throws IllegalArgumentException {
        manager.notify("bonuses", null, clientID, bonus);
    }
}
