package com.rostyslavprotsiv.model.entity.observer.listener;

import com.rostyslavprotsiv.model.entity.Client;
import com.rostyslavprotsiv.model.entity.bouquet.IBouquet;

public interface IEventListener {
    void update(String eventType, IBouquet bouquet, int clientID, int bonus);
}
