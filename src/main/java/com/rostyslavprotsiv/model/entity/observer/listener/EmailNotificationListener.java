package com.rostyslavprotsiv.model.entity.observer.listener;

import com.rostyslavprotsiv.model.entity.Client;
import com.rostyslavprotsiv.model.entity.bouquet.IBouquet;
import com.rostyslavprotsiv.model.entity.observer.publisher.Shop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.util.Map;

public class EmailNotificationListener extends AbstrEventListener {
    private static final Logger LOGGER = LogManager
            .getLogger(EmailNotificationListener.class);

    public EmailNotificationListener(Shop shop, Client client) {
        super(shop, client);
    }

    @Override
    public void update(String eventType, IBouquet bouquet, int clientID,
                       int bonus) {
        switch (eventType) {
            case "new" :
                LOGGER.info("Sending email (" + client.getEmail()
                        + ")about adding new bouquet type:"
                        + bouquet + " to " + client.getName());
                break;
            case "ready" :
                if (client.getId() == clientID) {
                    LOGGER.info("Sending email (" + client.getEmail()
                            + ")that his/her bouquet is ready :" + client
                    + ", bouquet : " + bouquet);
                }
                break;
            case "bonuses" :
                if (client.getId() == clientID) {
                    client.addBonuses(bonus);
                    LOGGER.info("Sending email (" + client.getEmail()
                            + ")that he/she has got some bonus :" + client
                            + ", bonus : " + bonus);
                }
                break;
            default :
                throw new IllegalArgumentException("There is no such "
                        + "subscription");
        }
    }
}
