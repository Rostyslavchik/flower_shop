package com.rostyslavprotsiv.model.entity.observer.listener;

import com.rostyslavprotsiv.model.entity.Client;
import com.rostyslavprotsiv.model.entity.bouquet.IBouquet;
import com.rostyslavprotsiv.model.entity.observer.publisher.Shop;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class ConsoleOutputListener extends AbstrEventListener {
    private static final Logger LOGGER = LogManager
            .getLogger(ConsoleOutputListener.class);

    public ConsoleOutputListener(Shop shop, Client client) {
        super(shop, client);
    }

    @Override
    public void update(String eventType, IBouquet bouquet, int clientID,
                       int bonus) {
        switch (eventType) {
            case "new" :
                LOGGER.debug("Output to the console "
                        +  "about adding new bouquet type:"
                        + bouquet + " to " + client.getName());
                break;
            case "ready" :
                if (client.getId() == clientID) {
                    LOGGER.debug("Output to the console "
                            + "that bouquet is ready :" + client
                            + ", bouquet : " + bouquet);
                }
                break;
            case "bonuses" :
                if (client.getId() == clientID) {
                    client.addBonuses(bonus);
                    LOGGER.debug("Output to the console"
                            + "that client received some bonus :" + client
                            + ", bonus : " + bonus);
                }
                break;
            default :
                throw new IllegalArgumentException("There is no such "
                        + "subscription");
        }
    }
}
