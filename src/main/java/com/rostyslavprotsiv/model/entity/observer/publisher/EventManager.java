package com.rostyslavprotsiv.model.entity.observer.publisher;

import com.rostyslavprotsiv.model.entity.Client;
import com.rostyslavprotsiv.model.entity.bouquet.IBouquet;
import com.rostyslavprotsiv.model.entity.observer.listener.IEventListener;

import java.util.*;

public class EventManager {
    private Map<String, List<IEventListener>> listeners = new HashMap<>();

    public EventManager(String... events) {
        for (String event : events) {
            listeners.put(event, new ArrayList<>());
        }
    }

    public void subscribe(String eventType, IEventListener listener) {
        List<IEventListener> users = listeners.get(eventType);
        users.add(listener);
    }

    public void unsubscribe(String eventType, IEventListener listener) {
        List<IEventListener> users = listeners.get(eventType);
        users.remove(listener);
    }

    public void notify(String eventType, IBouquet bouquet, int clientID,
                       int bonus) {
        List<IEventListener> users = listeners.get(eventType);
        for (IEventListener listener : users) {
            listener.update(eventType, bouquet, clientID, bonus);
        }
    }
}
