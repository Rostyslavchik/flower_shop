package com.rostyslavprotsiv.model.entity.observer.listener;

import com.rostyslavprotsiv.model.entity.Client;
import com.rostyslavprotsiv.model.entity.observer.publisher.Shop;

public abstract class AbstrEventListener implements IEventListener {
    protected Shop shop;
    protected Client client;

    public AbstrEventListener(Shop shop, Client client) {
        this.shop = shop;
        this.client = client;
    }
}
