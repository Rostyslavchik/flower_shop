package com.rostyslavprotsiv.model.exception;

public class FlowerLogicalException extends Exception {
    public FlowerLogicalException() {}

    public FlowerLogicalException(String message, Throwable cause) {
        super(message, cause);
    }

    public FlowerLogicalException(String message) {
        super(message);
    }

    public FlowerLogicalException(Throwable cause) {
        super(cause);
    }
}
