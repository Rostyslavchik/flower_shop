package com.rostyslavprotsiv.model.exception;

public class BouquetLogicalException extends Exception {
    public BouquetLogicalException() {
    }

    public BouquetLogicalException(String s) {
        super(s);
    }

    public BouquetLogicalException(String s, Throwable throwable) {
        super(s, throwable);
    }

    public BouquetLogicalException(Throwable throwable) {
        super(throwable);
    }
}
