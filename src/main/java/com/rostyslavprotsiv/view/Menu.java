package com.rostyslavprotsiv.view;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class Menu {
    private static final Logger LOGGER = LogManager.getLogger(Menu.class);

    public void welcome() {
        LOGGER.info("Welcome to my program!!");
    }

    public void outFactoryMethodWork() {
        LOGGER.info("Result of Factory Method work:");
    }

    public void outStrategyWork() {
        LOGGER.info("Result of Strategy work:");
    }

    public void outDecoratorWork() {
        LOGGER.info("Result of Decorator work:");
    }

    public void outObserverWork() {
        LOGGER.info("Result of Observer work:");
    }

    public void outAdapterWork() {
        LOGGER.info("Result of Adapter work:");
    }
}
