package com.rostyslavprotsiv.controller;

import com.rostyslavprotsiv.model.entity.Client;
import com.rostyslavprotsiv.model.entity.adapter.CardConverter;
import com.rostyslavprotsiv.model.entity.adapter.adaptee.ISpecificClientCard;
import com.rostyslavprotsiv.model.entity.adapter.adaptee.SocialCard;
import com.rostyslavprotsiv.model.entity.adapter.targett.BonusCard;
import com.rostyslavprotsiv.model.entity.adapter.targett.GoldenCard;
import com.rostyslavprotsiv.model.entity.adapter.targett.IClientCard;
import com.rostyslavprotsiv.model.entity.bouquet.BouquetType;
import com.rostyslavprotsiv.model.entity.bouquet.IBouquet;
import com.rostyslavprotsiv.model.entity.decorator.*;
import com.rostyslavprotsiv.model.entity.factory.BouquetBirthdayFactory;
import com.rostyslavprotsiv.model.entity.factory.BouquetFuneralFactory;
import com.rostyslavprotsiv.model.entity.factory.BouquetValentinesDayFactory;
import com.rostyslavprotsiv.model.entity.factory.BouquetWeddingFactory;
import com.rostyslavprotsiv.model.entity.observer.listener.ConsoleOutputListener;
import com.rostyslavprotsiv.model.entity.observer.listener.EmailNotificationListener;
import com.rostyslavprotsiv.model.entity.observer.listener.IEventListener;
import com.rostyslavprotsiv.model.entity.observer.publisher.Shop;
import com.rostyslavprotsiv.view.Menu;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

import java.time.Period;

public class Controller {
    private static final Logger LOGGER = LogManager.getLogger(Controller.class);
    private static final Menu MENU = new Menu();
    private static final BouquetBirthdayFactory BOUQUET_BIRTHDAY_FACTORY
            = new BouquetBirthdayFactory();
    private static final BouquetFuneralFactory BOUQUET_FUNERAL_FACTORY
            = new BouquetFuneralFactory();
    private static final BouquetValentinesDayFactory BOUQUET_VALENTINES_DAY_FACTORY
            = new BouquetValentinesDayFactory();
    private static final BouquetWeddingFactory BOUQUET_WEDDING_FACTORY
            = new BouquetWeddingFactory();
    private static final Shop SHOP = new Shop();

    public void execute() {
        MENU.welcome();
        demonstrateFactoryMethodAndStrategy();
        demonstrateDecorator();
        demonstrateObserver();
        demonstrateAdapter();
    }

    private void demonstrateFactoryMethodAndStrategy() {
        MENU.outFactoryMethodWork();
        IBouquet bouquet1 = BOUQUET_BIRTHDAY_FACTORY.compose(
                BouquetType.BIRTHDAY_LOVE);
        LOGGER.info(bouquet1);
        MENU.outStrategyWork();
        bouquet1.pack();
        bouquet1.deliver();
    }

    private void demonstrateDecorator() {
        MENU.outDecoratorWork();
        IBouquet bouquet1 = BOUQUET_FUNERAL_FACTORY.compose(
                BouquetType.FUNERAL_FRIEND);
        LOGGER.info(bouquet1);
        LOGGER.info(bouquet1.computeTotalPrice());
        BouquetDecorator delivery = new Delivery(Period.ofDays(2));
        BouquetDecorator discount = new Discount();
        BouquetDecorator packing = new Packing(PackingType.PAPER);
        delivery.setBouquet(bouquet1);
        discount.setBouquet(delivery);
        packing.setBouquet(discount);
        packing.pack();
        packing.compose();
        packing.deliver();
        packing.prepareBefore();
        LOGGER.info(packing.computeTotalPrice());
    }

    private void demonstrateObserver() {
        MENU.outObserverWork();
        Client client1 = new Client(123, "John", 3, "john@gmail.com");
        Client client2 = new Client(321, "Me", 0, "me@gmail.com");
        Client client3 = new Client(344, "Khristina", 2, "khristina@gmail.com");
        IBouquet bouquet1 = BOUQUET_WEDDING_FACTORY.compose(
                BouquetType.WEDDING_BRO);
        IEventListener listener1 = new ConsoleOutputListener(SHOP, client1);
        IEventListener listener2 = new EmailNotificationListener(SHOP, client2);
        IEventListener listener3 = new EmailNotificationListener(SHOP, client3);
        SHOP.getManager().subscribe("new",listener1);
        SHOP.getManager().subscribe("ready",listener1);
        SHOP.getManager().subscribe("new",listener2);
        SHOP.getManager().subscribe("ready",listener2);
        SHOP.getManager().subscribe("bonuses",listener2);
        SHOP.getManager().subscribe("bonuses",listener3);
        SHOP.addNewBouquet(bouquet1);
        SHOP.addReadyBouquet(bouquet1, 123);
        SHOP.addBonus(344, 19);
    }

    private void demonstrateAdapter() {
        MENU.outAdapterWork();
        IClientCard card1 = new BonusCard(4);
        IClientCard card2 = new GoldenCard(15.5);
        outAmount(card1);
        outAmount(card2);
        ISpecificClientCard specificClientCard = new SocialCard(8);
        CardConverter cardConverter = new CardConverter(specificClientCard);
        outAmount(cardConverter);
    }

    private void outAmount(IClientCard card) {
        LOGGER.info(card.getDiscount());
    }
}
